import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { BaseService, Method } from "./base.service";
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseService  {
  
  constructor(httpClient: HttpClient) {
    super(httpClient, 'user');
  }

  
  // Get a list of users
  public GetUsers(){
    return this.getApi();
  }

  // Create one or more users
  public CreateUsers(users: [User]){
    return this.postApi(users);
  }

  // Delete one or more users
  public DeleteUsers(userIds: [string]){
    return this.deleteApi(userIds);
  }

  /**
   * Get a specific user
   * @param userId 
   */
  public Get(userId: string){
    return this.getApi(null, userId);
  }

  /**
   *  Update a user
   * @param user 
   */
  public Update(user: User){
    return this.putApi(user, user._id);
  }

  /**
   * Delete a user
   * @param userId 
   */
  public Delete(userId: string){
    return this.deleteApi(userId);
  }

  /**
   * Change password
   * @param user 
   */
  public ChangePassword(user: User){
    return this.putApi(user, `${user._id}/password`);
  }

  public GetGroups(user: User){
    return this.getApi({id : user._id} , `${user.ownerId}/group`)
  }

}
