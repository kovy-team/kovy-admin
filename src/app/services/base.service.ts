import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  protected httpClient: HttpClient;
  protected entity: string;
  protected endpoint: any;
  constructor(httpClient: HttpClient, entity: string) {
    this.httpClient = httpClient;
    this.entity = entity;
    this.endpoint = environment.API_ENDPOINT;
  }

  protected baseUrl(): string {
    return `${this.endpoint}/${this.entity}`;
  }

  protected callApi<T>(path:string = null, method: Method = Method.GET, request: T = null){

    var apiUrl = path != null ? `${this.baseUrl()}/${path}` : this.baseUrl();
    if(method == Method.GET){
      return this.httpClient.get(apiUrl, request);
    }
    if(method == Method.POST){
      return this.httpClient.post(apiUrl, request);
    }
    if(method == Method.PUT){
      return this.httpClient.put(apiUrl, request);
    }
    if(method == Method.DELETE){
      return this.httpClient.delete(apiUrl, request);
    }
  }

  protected getApi(request?: HttpParams | {[param: string]: string | string[];}, path?:string){
    return this.callApi<HttpParams | {[param: string]: string | string[];}>(path, Method.GET, request);
  }

  protected postApi<T>(request?: T, path?:string){
    return this.callApi<T>(path, Method.POST, request);
  }

  protected putApi<T>(request?: T, path?:string){
    return this.callApi<T>(path, Method.PUT, request);
  }

  protected deleteApi<T>(request?: T, path?:string){
    return this.callApi<T>(path, Method.DELETE, request);
  }

}

export enum Method {
  GET,
  POST,
  PUT,
  DELETE
}
