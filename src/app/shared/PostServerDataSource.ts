import { Observable } from 'rxjs/internal/Observable';
import { ServerDataSource, } from 'ng2-smart-table';

import { HttpClient } from '@angular/common/http';
import { CustomServerDataSource } from "./CustomServerDataSource";
import { ServerSourceConf } from 'ng2-smart-table/lib/lib/data-source/server/server-source.conf';



export class PostServerDataSource<T> extends CustomServerDataSource<T> {
  protected params: any = {}

  constructor(http: HttpClient, conf?: ServerSourceConf | {}, params?: any){
    super(http, conf);
    if(params)
      this.setParams(params)
  }

  protected requestElements(): Observable<any> {
    let httpParams = this.createRequesParams();
    let requestBody = {};
    httpParams.keys().map(x => {
      if (x == 'pageIndex')
      {
        requestBody[x] = parseInt(httpParams.get(x)) - 1;
      } else
      {
        requestBody[x] = httpParams.get(x);
      }
    });

    let params = this.params;
    for (var key in params) {
      if (params.hasOwnProperty(key)) {
        requestBody[key] = this.params[key];
      }
    }

    return this.http.post(this.conf.endPoint, requestBody, { observe: 'response', responseType: 'json' });
  }

  public setParams(params: any) {
    this.params = params;
  }

}
