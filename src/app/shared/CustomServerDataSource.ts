import { Observable } from 'rxjs/internal/Observable';
import { ServerDataSource, } from 'ng2-smart-table';

import { HttpClient } from '@angular/common/http';



export class CustomServerDataSource<T> extends ServerDataSource {

  getAllItems(): T[] {
    return this.data.slice(0);
  }
}
