import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { UsersRoutingModule } from './users-routing.module';
import { NbButtonModule, NbCardModule, NbInputModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule } from '@angular/forms';
import { ThemeModule } from '../../@theme/theme.module';



@NgModule({
  declarations: [
    UsersComponent,
    UserManagementComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    NbCardModule,
    NbInputModule,
    NbButtonModule,
    ThemeModule,
    Ng2SmartTableModule,
    UsersRoutingModule
  ]
})
export class UsersModule { }
