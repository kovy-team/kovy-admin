import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { UserManagementComponent } from './user-management/user-management.component';

const routes: Routes = [{
  path: '',
  component: UsersComponent,
  children: [
    {
      path: '',
      component: UserManagementComponent,
    },
    // {
    //   path: 'detail/:id',
    //   component: OrderDetailComponent,
    // },
    // {
    //   path: 'sale',
    //   component: MachineSaleComponent,
    // },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule { }
