import { Component, OnInit } from '@angular/core';
import { PostServerDataSource } from '../../../shared/postServerDataSource';
import { User } from '../../../models/user';

@Component({
  selector: 'ngx-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
  settings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: false,
      position: 'right', // left|right
    },
    filter: {
      inputClass: 'hidden'
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    pager: {
      display: true,
      perPage: 20,
    },
    columns: {
      userName: {
        title: 'Username',
        type: 'string',
        filter: false,
      },
      email: {
        title: 'Email',
        type: 'string',
        filter: false,
      },
      createdDate: {
        title: 'Created Date',
        type: 'string',
        filter: false,
      },
      modifiedDate: {
        title: 'Modified Date',
        type: 'string',
        filter: false,
      },
      // dataStatus: {
      //   title: 'Status',
      //   type: 'custom',
      //   filter: false,
      //   width: '100px', 
      //   renderComponent: DataStatusComponent,
      // },
      // action: {
      //   title: 'Action',
      //   filter: false,
      //   type: 'custom',
      //   width: '280px',  
      //   renderComponent: UserActionsComponent,
      // }
    },
  };
  source: PostServerDataSource<User>;

  constructor() { }

  ngOnInit(): void {
  }

  createUser(){

  }

  onDeleteConfirm(event): void {
    // this.service.getCurrentUser().subscribe((user) => {
    //   console.log(user);
    // });
    // if (window.confirm('Are you sure you want to delete?')) {
    //   event.confirm.resolve();
    // } else {
    //   event.confirm.reject();
    // }
    }

}
